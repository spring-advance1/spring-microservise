package com.kshrd.example.taskservice

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class Main {
}

fun main(array: Array<String>){
    runApplication<Main>(
        *array
    )
}
