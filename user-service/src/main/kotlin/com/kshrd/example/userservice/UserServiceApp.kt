package com.kshrd.example.userservice

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
open class UserServiceApp
fun main(array: Array<String>){
    runApplication<UserServiceApp>(
       *array
   )
}
