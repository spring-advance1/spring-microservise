package com.example.springmicroservicesminiproject

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SpringMicroservicesMiniProjectApplication

fun main(args: Array<String>) {
    runApplication<SpringMicroservicesMiniProjectApplication>(*args)
}
